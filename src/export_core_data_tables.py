#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import sys
import time

from utils import export_utils, db_utils


table_list = ['CAM_ACE', 'CAM_ACM', 'CAM_APA', 'CAM_APR', 'CAM_APS', 'CAM_APT', 'CAM_ARC', 'CAM_ARN', 'CAM_ARQ', 'CAM_ARR', 'CAM_ART', 'CAM_AST', 'CAM_ATC',
              'CAM_ATE', 'CAM_AWB', 'CAM_AWC', 'CAM_AWR', 'CAM_AWS', 'CAM_AYW', 'CAM_CLA', 'CAM_CSC', 'CAM_CSR', 'CAM_CSS', 'CAM_DYR', 'CAM_EXC', 'CAM_FMC',
              'CAM_FME', 'CAM_FML', 'CAM_IJR', 'CAM_LEV', 'CAM_LTL', 'CAM_LVC', 'CAM_LVE', 'CAM_MAB', 'CAM_MAC', 'CAM_MAD', 'CAM_MAP', 'CAM_MAV', 'CAM_MAVT',
              'CAM_MDS', 'CAM_MGC', 'CAM_MGE', 'CAM_MKC', 'CAM_MKS', 'CAM_MMB', 'CAM_MMR', 'CAM_MOT', 'CAM_MSD', 'CAM_MSS', 'CAM_MTC', 'CAM_MTE', 'CAM_NTT',
              'CAM_PAW', 'CAM_PDI', 'CAM_PDM', 'CAM_PDR', 'CAM_PDT', 'CAM_PDV', 'CAM_PIC', 'CAM_PIE', 'CAM_PIT', 'CAM_PUC', 'CAM_PUE', 'CAM_RAF', 'CAM_RCA',
              'CAM_RCL', 'CAM_REX', 'CAM_RFG', 'CAM_RFL', 'CAM_RFO', 'CAM_RFT', 'CAM_RHC', 'CAM_RMC', 'CAM_RME', 'CAM_RRD', 'CAM_RTC', 'CAM_RUT', 'CAM_SES',
              'CAM_SHS', 'CAM_SLE', 'CAM_TMR', 'CAM_TUS', 'CAM_VRE', 'CAM_VRS', 'CAM_WJR', 'CAM_WRM', 'CAM_WRS', 'CAM_WSC', 'CAM_WSD', 'CAM_WSE', 'CAM_WSL',
              'CAM_WSM', 'CAM_WSP', 'CAM_YPD', 'INS_AA1', 'INS_AA2', 'INS_AA3', 'INS_AA4', 'INS_AA5', 'INS_AVG', 'INS_AWD', 'INS_AYR', 'INS_BLD', 'INS_CCO',
              'INS_CMP', 'INS_DPT', 'INS_EQA', 'INS_ESB', 'INS_FPE', 'INS_FPT', 'INS_INS', 'INS_LCA', 'INS_MOA', 'INS_MOD', 'INS_PRD', 'INS_PRG', 'INS_PRU',
              'INS_PSL', 'INS_PWT', 'INS_PWY', 'INS_ROE', 'INS_ROM', 'INS_ROU', 'INS_RTY', 'INS_SCH', 'INS_SIT', 'INS_STS', 'INS_SUB', 'INS_VAL', 'INS_YPS',
              'MEN_AFL', 'MEN_ARD', 'MEN_ASE', 'MEN_ATY', 'MEN_CAT', 'MEN_CGR', 'MEN_COL', 'MEN_DEF', 'MEN_DEL', 'MEN_DET', 'MEN_DMB', 'MEN_DMO', 'MEN_DOR',
              'MEN_DRI', 'MEN_DTA', 'MEN_DTC', 'MEN_ERE', 'MEN_FEX', 'MEN_FTS', 'MEN_FTY', 'MEN_HTV', 'MEN_ICO', 'MEN_IED', 'MEN_IFD', 'MEN_IMG', 'MEN_LAG',
              'MEN_LCD', 'MEN_LGA', 'MEN_LUD', 'MEN_MCG', 'MEN_MES', 'MEN_MHV', 'MEN_MNB', 'MEN_MRA', 'MEN_MRC', 'MEN_MRQ', 'MEN_MSC', 'MEN_MVD', 'MEN_ODT',
              'MEN_ORG', 'MEN_ORT', 'MEN_PAT', 'MEN_PEA', 'MEN_PGR', 'MEN_RET', 'MEN_RGF', 'MEN_SLL', 'MEN_SMG', 'MEN_SSC', 'MEN_SYP', 'MEN_TIC', 'MEN_UDD',
              'MEN_UDF', 'MEN_UDV', 'MEN_UGC', 'MEN_UGR', 'MEN_ULL', 'MEN_WSF', 'MEN_XCON', 'MEN_XET', 'MEN_XON', 'MEN_XPT', 'MEN_XST', 'SRS_AAS', 'SRS_AAT',
              'SRS_ACC', 'SRS_AES', 'SRS_AFC', 'SRS_AGB', 'SRS_AGN', 'SRS_APC', 'SRS_ARI', 'SRS_ASSP', 'SRS_CBK', 'SRS_CBO', 'SRS_CBS', 'SRS_CCG', 'SRS_CCL',
              'SRS_CCP', 'SRS_CDD', 'SRS_CGP', 'SRS_CMY', 'SRS_COB', 'SRS_COD', 'SRS_CPC', 'SRS_CPP', 'SRS_CRD', 'SRS_CRS', 'SRS_CTY', 'SRS_CUC', 'SRS_CWS',
              'SRS_CYB', 'SRS_CYC', 'SRS_DCA', 'SRS_DCD', 'SRS_DCG', 'SRS_DCL', 'SRS_DDG', 'SRS_DEC', 'SRS_DEE', 'SRS_DES', 'SRS_DRE', 'SRS_DRP', 'SRS_DRT',
              'SRS_DSA', 'SRS_DSC', 'SRS_DSG', 'SRS_DST', 'SRS_DTC', 'SRS_DTT', 'SRS_DVN', 'SRS_EAT', 'SRS_EFL', 'SRS_EFU', 'SRS_EGR', 'SRS_ELA', 'SRS_ELG',
              'SRS_ELT', 'SRS_EQE', 'SRS_ERQ', 'SRS_ESG', 'SRS_ESM', 'SRS_ESU', 'SRS_ETH', 'SRS_ETY', 'SRS_EVT', 'SRS_EXM', 'SRS_EXT', 'SRS_FAC', 'SRS_FAP',
              'SRS_FCA', 'SRS_FCE', 'SRS_FCR', 'SRS_FDF', 'SRS_FDG', 'SRS_FDT', 'SRS_FNC', 'SRS_FND', 'SRS_FNF', 'SRS_FNH', 'SRS_FNK', 'SRS_FNL', 'SRS_FNN',
              'SRS_FNO', 'SRS_FNP', 'SRS_FNS', 'SRS_FNV', 'SRS_FSG', 'SRS_FST', 'SRS_GEG', 'SRS_GEN', 'SRS_HQL', 'SRS_IAC', 'SRS_IBC', 'SRS_IBF', 'SRS_IBH',
              'SRS_IBL', 'SRS_IBMR', 'SRS_IBSD', 'SRS_IDT', 'SRS_IFC', 'SRS_IGA', 'SRS_IGR', 'SRS_IOD', 'SRS_IPD', 'SRS_IPJ', 'SRS_IPN', 'SRS_IPO', 'SRS_IPON',
              'SRS_IPP', 'SRS_IPPN', 'SRS_IPPQ', 'SRS_IPQ', 'SRS_LAE', 'SRS_LAR', 'SRS_LCE', 'SRS_LDH', 'SRS_LDT', 'SRS_LPS', 'SRS_LRC', 'SRS_MCR', 'SRS_MDF',
              'SRS_MEA', 'SRS_MER', 'SRS_MOB', 'SRS_MOS', 'SRS_MPE', 'SRS_MPM', 'SRS_MTH', 'SRS_NAC', 'SRS_NAT', 'SRS_NKR', 'SRS_NSL', 'SRS_OHC', 'SRS_OHP',
              'SRS_OHS', 'SRS_OLB', 'SRS_OLT', 'SRS_OTY', 'SRS_PCM', 'SRS_PDT', 'SRS_PID', 'SRS_PJT', 'SRS_PRE', 'SRS_PVC', 'SRS_PYM', 'SRS_QEN', 'SRS_QST',
              'SRS_QUC', 'SRS_QUE', 'SRS_QUL', 'SRS_RAA', 'SRS_REG', 'SRS_RFT', 'SRS_RSP', 'SRS_SCL', 'SRS_SFC', 'SRS_SFT', 'SRS_SHT', 'SRS_SOI', 'SRS_SQL',
              'SRS_SQS', 'SRS_SQT', 'SRS_SSL', 'SRS_STA', 'SRS_STD', 'SRS_STY', 'SRS_SUC', 'SRS_SUE', 'SRS_TCM', 'SRS_TFC', 'SRS_TFE', 'SRS_TFG', 'SRS_TFV',
              'SRS_TGF', 'SRS_TIF', 'SRS_TTL', 'SRS_UCF', 'SRS_UEN', 'SRS_UOM', 'SRS_USU', 'SRS_VAR', 'SRS_VAT', 'SRS_VCO', 'SRS_VDR']

directory_path = 'core_data_export'
if not os.path.exists(directory_path):
    os.makedirs(directory_path)
        
with db_utils.db_connect():
    for table in table_list:
        file_path = os.path.join(directory_path, "%s.xml" % table)
        
        sys.stdout.write('Exporting table: %s ... ' % table)
        time1 = time.time()
        export_utils.export_table_to_single_xml_stream(table, file_path)
        time2 = time.time()
        sys.stdout.write('took %0.3f ms\n' % ((time2 - time1) * 1000.0))

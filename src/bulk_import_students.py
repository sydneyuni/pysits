#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os

from utils import stu_utils, db_utils


def load_with_id(file_path, identifier):
    student = stu_utils.read_student_from_file(file_path)
    stu_utils.set_student_identifier(student, identifier)
    return student

students = []

i = 10000
for f in os.listdir("students"):
    if f.endswith(".json"):
        students.append(load_with_id("students/%s" % f, i))
        i += 1

with db_utils.db_connect():
    stu_utils.bulk_import(students)

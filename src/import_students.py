#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import sys

from utils import stu_utils, db_utils


if len(sys.argv) < 2:
    print "Usage: %s [DIRECTORY]" % sys.argv[0]
    sys.exit(1)

dir_path = sys.argv[1]
if not os.path.isdir(dir_path):
    print "Directory does not exist: %s [DIRECTORY]" % dir_path
    sys.exit(1)

with db_utils.db_connect():
    for f in os.listdir(dir_path):
        if f.endswith(".json"):
            student = stu_utils.read_student_from_file("%s/%s" % (dir_path, f))
            stu_utils.import_student(student)

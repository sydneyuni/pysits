#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os
import sys
import time

from utils import export_utils, db_utils

if len(sys.argv) < 2:
    print "Usage: %s [DESTINATION]" % sys.argv[0]
    sys.exit(1)

directory_path = sys.argv[1]
if not os.path.exists(directory_path):
    os.makedirs(directory_path)
        
with open('config_entities.txt') as f:
    table_list = f.read().splitlines()

with db_utils.db_connect():
    for table in table_list:
        file_path = os.path.join(directory_path, "%s.xml" % table)
        
        sys.stdout.write('Exporting table: %s ... ' % table)
        time1 = time.time()
        export_utils.export_table_to_single_xml_stream(table, file_path)
        time2 = time.time()
        sys.stdout.write('took %0.3f ms\n' % ((time2 - time1) * 1000.0))

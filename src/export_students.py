#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import os

from utils import stu_utils, db_utils


with db_utils.db_connect():
    directory_path = 'stu-export'
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    sql = "SELECT stu_code FROM (SELECT stu_code, stu_sta2 FROM ins_stu SAMPLE(10)) WHERE stu_sta2 IN ('SA', 'SO')"
    result = db_utils.fetch(sql)
    for row in result:
        student = stu_utils.get_student(row['stu_code'])
        file_path = '%s/%s.json' % (directory_path, row['stu_code'])
        stu_utils.write_student_to_file(student, file_path)

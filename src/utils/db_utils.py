#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from contextlib import contextmanager
import datetime
import os

import cx_Oracle


DB_DETAILS_FILE = '/path/to/sits/vision/swupdate/config/sits-ora.txt'

DB_LOCAL_CONNECTION = {
    'host': 'sitsvm.example.com',
    'port': '1521',
    'service': 'sits',
    'username': 'sits',
    'password': 'sits',
    'schema': 'SITS',
}

_database_connection = None

@contextmanager
def db_connect():
    global _database_connection
    
    try:
        if os.path.isfile(DB_DETAILS_FILE):
            with open(DB_DETAILS_FILE, 'r') as f:
                db_details = f.readline().split(',')
    
            username = db_details[0]
            password = db_details[1]
            tns = db_details[2]
            schema = username
        else:
            username = DB_LOCAL_CONNECTION['username']
            password = DB_LOCAL_CONNECTION['password']
            tns = cx_Oracle.makedsn(DB_LOCAL_CONNECTION['host'], DB_LOCAL_CONNECTION['port'], service_name=DB_LOCAL_CONNECTION['service'])
            schema = DB_LOCAL_CONNECTION['schema']
    
        os.environ["NLS_LANG"] = 'ENGLISH_AUSTRALIA.AL32UTF8'

        _database_connection = cx_Oracle.connect(username, password, tns)
        _database_connection.current_schema = schema
        
        yield _database_connection
    except cx_Oracle.DatabaseError as e:
        print('Database connection error: %s' % format(e))
        raise
    finally:
        _database_connection.close()
        _database_connection = None

def get_db_connection():
    global _database_connection
    
    if _database_connection:
        return _database_connection
    else:
        raise ValueError('Database is not connected!')

@contextmanager
def db_cursor(close_cursor=True):
    try:
        connection = get_db_connection()
        cursor = connection.cursor()
        cursor.arraysize = 50
        yield cursor
    finally:
        if close_cursor:
            cursor.close()

def fetch(sql, value_dict={}, preprocess_rows=True):
    value_dict = _clean_oracle_params(value_dict)
    
    with db_cursor(preprocess_rows) as cursor:
        try:
            cursor.execute(sql, **value_dict)
        except Exception:
            print('Exception with SQL: %s' % sql)
            print(value_dict)
            raise
        
        if preprocess_rows:
            result = []
            for row in cursor:
                result.append(process_row(row, cursor))
            return result
        else:
            return cursor

# http://stackoverflow.com/questions/10455863/
# 'enumerate' method required due to SITS excessive use of LOB columns
def process_row(row, cursor):
    columns = [d[0] for d in cursor.description]
    row_dict = {}
    
    for i, col in enumerate(columns):
        if type(row[i]) == cx_Oracle.LOB:
            row_dict[col.lower()] = row[i].read()
        else:
            row_dict[col.lower()] = row[i]
    
    return row_dict

def update(sql, value_dict={}, input_sizes=None):
    value_dict = _clean_oracle_params(value_dict)
    
    connection = get_db_connection()
    with db_cursor() as cursor:
        cursor.setinputsizes(**input_sizes)
    
        try:
            cursor.execute(sql, **value_dict)
        except Exception:
            print('Exception with SQL: %s' % sql)
            print(value_dict)
            raise

    connection.commit()

def update_multiple(sql, rows, input_sizes):
    for i in range(0, len(rows)):
        rows[i] = _clean_oracle_params(rows[i])

    connection = get_db_connection()
    with db_cursor() as cursor:
        cursor.setinputsizes(**input_sizes)
    
        max_size = 50000
        if(len(rows) > max_size):
            # http://stackoverflow.com/questions/312443/
            row_list = [rows[i:i + max_size] for i in xrange(0, len(rows), max_size)]
            for chunk in row_list:
                try:
                    cursor.executemany(sql, chunk)
                except Exception:
                    print('Exception with SQL: %s' % sql)
                    raise
        else:
            try:
                cursor.executemany(sql, rows)
            except Exception:
                print('Exception with SQL: %s' % sql)
                raise
        
    connection.commit()

def _clean_oracle_params(params_dict):
    '''Final corrections to SQL parameters.
    cx_Oracle does not support any 'date/time' related objects apart from 'datetime'.
    SITS' date & time DB columns are always 'DATE', requiring further adjustments 
    '''
    if not params_dict:
        return params_dict

    for key, value in params_dict.items():
        if type(value) is datetime.date:
            params_dict[key] = datetime.datetime.combine(value, datetime.time.min)
        elif type(value) is datetime.time:
            params_dict[key] = datetime.datetime.combine(datetime.date(1900, 1, 1), value)
    return params_dict

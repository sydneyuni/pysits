#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import codecs
import datetime
import json
import random
import re
import uuid

from dateutil import parser
from dateutil.relativedelta import relativedelta

from utils import generic_utils
from utils import sits_utils


sample_data = {}
with codecs.open('data/FNM1.dat', encoding='utf-8') as f:
    sample_data['fnm1'] = f.read().splitlines()
with codecs.open('data/FNM2.dat', encoding='utf-8') as f:
    sample_data['fnm2'] = f.read().splitlines()
with codecs.open('data/FNM3.dat', encoding='utf-8') as f:
    sample_data['fnm3'] = f.read().splitlines()
with codecs.open('data/FUSD.dat', encoding='utf-8') as f:
    sample_data['fusd'] = f.read().splitlines()
with codecs.open('data/SURN.dat', encoding='utf-8') as f:
    sample_data['surn'] = f.read().splitlines()

def get_student(stu_code):
    student = {}
    
    mst_code = sits_utils.get_single_field('men_mre', 'mre_mstc', {'mre_code': stu_code, 'mre_mrcc': 'STU'})
    ipu_code = sits_utils.get_single_field('men_mre', 'mre_code', {'mre_mstc': mst_code, 'mre_mrcc': 'IPU'})
    
    student['ins_stu'] = sits_utils.get_single_row('ins_stu', None, {'stu_code': stu_code})
    student['srs_mst'] = sits_utils.get_single_row('srs_mst', None, {'mst_code': mst_code})
    student['men_mre'] = sits_utils.get_rows('men_mre', None, {'mre_mstc': mst_code})

    student['srs_ipu'] = sits_utils.get_single_row('srs_ipu', None, {'ipu_code': ipu_code})
    student['srs_ipf'] = sits_utils.get_rows('srs_ipf', None, {'ipf_ipuc': ipu_code})
    student['srs_iprq'] = sits_utils.get_rows('srs_iprq', None, {'iprq_ipuc': ipu_code})
    student['men_mus'] = sits_utils.get_single_row('men_mus', None, {'mus_muac': ipu_code})
    student['srs_ipr'] = sits_utils.get_single_row('srs_ipr', None, {'ipr_ipuc': ipu_code})
    student['srs_ipa'] = sits_utils.get_rows('srs_ipa', None, {'ipa_ipuc': ipu_code})

    student['men_add'] = sits_utils.get_rows('men_add', None, {'add_mst1': mst_code})
    student['men_mua'] = sits_utils.get_rows('men_mua', None, {'mua_mstc': mst_code})
    student['srs_cap'] = sits_utils.get_rows('srs_cap', None, {'cap_stuc': stu_code})
    
    student['srs_drl'] = sits_utils.get_rows('srs_drl', None, {'drl_stuc': stu_code})
    student['srs_scc'] = sits_utils.get_rows('srs_scc', None, {'scc_stuc': stu_code})
    student['srs_apf'] = sits_utils.get_rows('srs_apf', None, {'apf_stuc': stu_code})
    student['srs_cdl'] = sits_utils.get_rows('srs_cdl', None, {'cdl_stuc': stu_code})
    student['srs_fcl'] = sits_utils.get_rows('srs_fcl', None, {'fcl_stuc': stu_code})
    student['srs_ppt'] = sits_utils.get_rows('srs_ppt', None, {'ppt_stuc': stu_code})
    student['srs_prf'] = sits_utils.get_rows('srs_prf', None, {'prf_stuc': stu_code})
    student['srs_coe'] = sits_utils.get_rows('srs_coe', None, {'coe_stuc': stu_code})

    student['srs_scj'] = sits_utils.get_rows('srs_scj', None, {'scj_stuc': stu_code})
    student['ins_spr'] = sits_utils.get_rows('ins_spr', None, {'spr_stuc': stu_code})
    student['srs_dbt'] = sits_utils.get_rows('srs_dbt', None, {'dbt_mstc': mst_code})
    
    student['srs_sce'] = []
    student['srs_fdu'] = []
    student['srs_fdv'] = []
    student['srs_rds'] = []
    student['srs_ssd'] = []
    student['srs_rdx'] = []
    for scj in student['srs_scj']:
        student['srs_sce'].extend(sits_utils.get_rows('srs_sce', None, {'sce_scjc': scj.get('scj_code')}))
        student['srs_fdu'].extend(sits_utils.get_rows('srs_fdu', None, {'fdu_scjc': scj.get('scj_code')}))
        student['srs_fdv'].extend(sits_utils.get_rows('srs_fdv', None, {'fdv_scjc': scj.get('scj_code')}))
        student['srs_rds'].extend(sits_utils.get_rows('srs_rds', None, {'rds_scjc': scj.get('scj_code')}))
        student['srs_ssd'].extend(sits_utils.get_rows('srs_ssd', None, {'ssd_scjc': scj.get('scj_code')}))
        student['srs_rdx'].extend(sits_utils.get_rows('srs_rdx', None, {'rdx_scjc': scj.get('scj_code')}))
    
    student['cam_sme'] = []
    student['cam_srh'] = []
    student['cam_ssn'] = []
    student['cam_sdd'] = []
    student['cam_sdl'] = []
    student['cam_smo'] = []
    student['cam_smot'] = []
    student['cam_sro'] = []
    student['cam_sms'] = []
    student['cam_smst'] = []
    student['cam_sph'] = []
    student['ins_smr'] = []
    student['ins_spy'] = []
    student['cam_sas'] = []
    for spr in student['ins_spr']:
        student['cam_sme'].extend(sits_utils.get_rows('cam_sme', None, {'sme_sprc': spr.get('spr_code')}))
        student['cam_srh'].extend(sits_utils.get_rows('cam_srh', None, {'srh_sprc': spr.get('spr_code')}))
        student['cam_ssn'].extend(sits_utils.get_rows('cam_ssn', None, {'ssn_sprc': spr.get('spr_code')}))
        student['cam_sdd'].extend(sits_utils.get_rows('cam_sdd', None, {'sdd_sprc': spr.get('spr_code')}))
        student['cam_sdl'].extend(sits_utils.get_rows('cam_sdl', None, {'sdl_sprc': spr.get('spr_code')}))
        student['cam_smo'].extend(sits_utils.get_rows('cam_smo', None, {'spr_code': spr.get('spr_code')}))
        student['cam_smot'].extend(sits_utils.get_rows('cam_smot', None, {'spr_code': spr.get('spr_code')}))
        student['cam_sro'].extend(sits_utils.get_rows('cam_sro', None, {'sro_sprc': spr.get('spr_code')}))
        student['cam_sms'].extend(sits_utils.get_rows('cam_sms', None, {'spr_code': spr.get('spr_code')}))
        student['cam_smst'].extend(sits_utils.get_rows('cam_smst', None, {'spr_code': spr.get('spr_code')}))
        student['cam_sph'].extend(sits_utils.get_rows('cam_sph', None, {'sph_sprc': spr.get('spr_code')}))
        student['ins_smr'].extend(sits_utils.get_rows('ins_smr', None, {'spr_code': spr.get('spr_code')}))
        student['ins_spy'].extend(sits_utils.get_rows('ins_spy', None, {'spy_sprc': spr.get('spr_code')}))
        student['cam_sas'].extend(sits_utils.get_rows('cam_sas', None, {'spr_code': spr.get('spr_code')}))
        
    student['srs_snk'] = sits_utils.get_rows('srs_snk', None, {'snk_stuc': stu_code})
    student['srs_ssp'] = sits_utils.get_rows('srs_ssp', None, {'ssp_stuc': stu_code})
    student['srs_tfn'] = sits_utils.get_rows('srs_tfn', None, {'tfn_stuc': stu_code})
    student['srs_caf'] = sits_utils.get_rows('srs_caf', None, {'caf_stuc': stu_code})

    student['srs_vis'] = sits_utils.get_rows('srs_vis', None, {'vis_stuc': stu_code})

    return student

def set_mst_code(student, mst_code):
    if student['srs_mst']:
        student['srs_mst']['mst_code'] = mst_code
    
    if student['men_mus']:
        student['men_mus']['mus_valu'] = re.sub(r'MST_CODE=\d+', 'MST_CODE=%s' % mst_code, student['men_mus']['mus_valu'])
    
    for record in student['men_mre']:
        record['mre_mstc'] = mst_code
        
    for record in student['men_add']:
        record['add_mst1'] = mst_code
        record['add_mst2'] = mst_code
        
    for record in student['men_mua']:
        record['mua_mstc'] = mst_code
    
    for record in student['srs_dbt']:
        record['dbt_mstc'] = mst_code
    
    for record in student['cam_smst']:
        record['sms_mstc'] = mst_code
    
    for record in student['srs_tfn']:
        record['tfn_mstc'] = mst_code
        record['tfn_mst1'] = mst_code
    
    for record in student['srs_caf']:
        record['caf_mstc'] = mst_code
    
    for record in student['cam_ssn']:
        record['ssn_mstc'] = mst_code

    for record in student['srs_scc']:
        record['scc_mstc'] = mst_code
    
    for record in student['srs_coe']:
        record['coe_mstc'] = mst_code

def set_stu_code(student, stu_code):
    if student['srs_mst']:
        student['srs_mst']['mst_adid'] = stu_code
    
    if student['ins_stu']:
        student['ins_stu']['stu_code'] = stu_code
        student['ins_stu']['stu_adid'] = stu_code
        student['ins_stu']['stu_udf7'] = ''
        student['ins_stu']['stu_udf9'] = ''
    
    if student['srs_ipr']:
        student['srs_ipr']['ipr_tcod'] = stu_code
        
    for record in student['men_mre']:
        if record['mre_mrcc'] == 'STU':
            record['mre_code'] = stu_code
    
    for record in student['men_mua']:
        if record['mua_udf1'] == 'STU':
            record['mua_code'] = stu_code

    for record in student['men_add']:
        record['add_adid'] = stu_code
        
    for record in student['srs_drl']:
        record['drl_stuc'] = stu_code
        
    for record in student['srs_scc']:
        record['scc_stuc'] = stu_code
        
    for record in student['srs_apf']:
        record['apf_stuc'] = stu_code
        
    for record in student['srs_cdl']:
        record['cdl_stuc'] = stu_code
        
    for record in student['srs_fcl']:
        record['fcl_stuc'] = stu_code
        
    for record in student['srs_ppt']:
        record['ppt_stuc'] = stu_code
        
    for record in student['srs_cap']:
        record['cap_stuc'] = stu_code
        record['cap_scjc'] = '%s/%s' % (stu_code, record['cap_scjc'].split('/')[1]) if record['cap_scjc'] else None if record['cap_scjc'] else None
    
    for record in student['srs_prf']:
        record['prf_guid'] = unicode(uuid.uuid4())
        record['prf_stuc'] = stu_code
    
    for record in student['srs_coe']:
        record['coe_code'] = generic_utils.random_string(12)
        record['coe_stuc'] = stu_code
        record['coe_scjc'] = '%s/%s' % (stu_code, record['coe_scjc'].split('/')[1]) if record['coe_scjc'] else None if record['coe_scjc'] else None
    
    for record in student['srs_dbt']:
        record['dbt_code'] = stu_code
        record['dbt_stuc'] = stu_code
    
    for record in student['srs_scj']:
        record['scj_stuc'] = stu_code
        record['scj_code'] = '%s/%s' % (stu_code, record['scj_code'].split('/')[1]) if record['scj_code'] else None if record['scj_code'] else None
        record['scj_sprc'] = '%s/%s' % (stu_code, record['scj_sprc'].split('/')[1]) if record['scj_sprc'] else None if record['scj_sprc'] else None
    
    for record in student['ins_spr']:
        record['spr_stuc'] = stu_code
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None if record['spr_code'] else None
    
    for record in student['srs_sce']:
        record['sce_stuc'] = stu_code
        record['sce_scjc'] = '%s/%s' % (stu_code, record['sce_scjc'].split('/')[1]) if record['sce_scjc'] else None if record['sce_scjc'] else None
    
    for record in student['cam_sme']:
        record['sme_sprc'] = '%s/%s' % (stu_code, record['sme_sprc'].split('/')[1]) if record['sme_sprc'] else None if record['sme_sprc'] else None
    
    for record in student['cam_srh']:
        record['srh_sprc'] = '%s/%s' % (stu_code, record['srh_sprc'].split('/')[1]) if record['srh_sprc'] else None if record['srh_sprc'] else None
    
    for record in student['cam_ssn']:
        record['ssn_sprc'] = '%s/%s' % (stu_code, record['ssn_sprc'].split('/')[1]) if record['ssn_sprc'] else None if record['ssn_sprc'] else None
    
    for record in student['cam_sdl']:
        record['sdl_sprc'] = '%s/%s' % (stu_code, record['sdl_sprc'].split('/')[1]) if record['sdl_sprc'] else None if record['sdl_sprc'] else None
    
    for record in student['srs_fdu']:
        record['fdu_scjc'] = '%s/%s' % (stu_code, record['fdu_scjc'].split('/')[1]) if record['fdu_scjc'] else None if record['fdu_scjc'] else None
        record['fdu_udf7'] = stu_code
    
    for record in student['srs_fdv']:
        record['fdv_scjc'] = '%s/%s' % (stu_code, record['fdv_scjc'].split('/')[1]) if record['fdv_scjc'] else None if record['fdv_scjc'] else None
        record['fdv_udf7'] = stu_code
    
    for record in student['cam_smo']:
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None if record['spr_code'] else None
        record['smo_scjc'] = '%s/%s' % (stu_code, record['smo_scjc'].split('/')[1]) if record['smo_scjc'] else None if record['smo_scjc'] else None
    
    for record in student['cam_smot']:
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None if record['spr_code'] else None
    
    for record in student['cam_sro']:
        record['sro_sprc'] = '%s/%s' % (stu_code, record['sro_sprc'].split('/')[1]) if record['sro_sprc'] else None if record['sro_sprc'] else None
    
    for record in student['cam_sms']:
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None if record['spr_code'] else None
        record['sms_scjc'] = '%s/%s' % (stu_code, record['sms_scjc'].split('/')[1]) if record['sms_scjc'] else None if record['sms_scjc'] else None
    
    for record in student['cam_smst']:
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None if record['spr_code'] else None
    
    for record in student['cam_sph']:
        record['sph_sprc'] = '%s/%s' % (stu_code, record['sph_sprc'].split('/')[1]) if record['sph_sprc'] else None if record['sph_sprc'] else None
    
    for record in student['ins_smr']:
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None if record['spr_code'] else None
        record['smr_scjc'] = '%s/%s' % (stu_code, record['smr_scjc'].split('/')[1]) if record['smr_scjc'] else None if record['smr_scjc'] else None
        record['smr_scnc'] = '%s/%s' % (stu_code, record['smr_scnc'].split('/')[1]) if record['smr_scnc'] else None if record['smr_scnc'] else None
    
    for record in student['ins_spy']:
        record['spy_sprc'] = '%s/%s' % (stu_code, record['spy_sprc'].split('/')[1]) if record['spy_sprc'] else None if record['spy_sprc'] else None
    
    for record in student['cam_sdd']:
        record['sdd_sprc'] = '%s/%s' % (stu_code, record['sdd_sprc'].split('/')[1]) if record['sdd_sprc'] else None if record['sdd_sprc'] else None
        
        if record['sdd_sprv']:
            record['sdd_sprv'] = re.sub(r'SPR_CODE=\d+/(\d+)', 'SPR_CODE=%s/\\1' % stu_code, record['sdd_sprv'])
            record['sdd_sprv'] = re.sub(r'SPR_STUC=\d+', 'SPR_STUC=%s' % stu_code, record['sdd_sprv'])
        
        if record['sdd_scjv']:
            record['sdd_scjv'] = re.sub(r'SCJ_CODE=\d+/(\d+)', 'SCJ_CODE=%s/\\1' % stu_code, record['sdd_scjv'])
            record['sdd_scjv'] = re.sub(r'SCJ_STUC=\d+', 'SCJ_STUC=%s' % stu_code, record['sdd_scjv'])
            record['sdd_scjv'] = re.sub(r'SCJ_SPRC=\d+/(\d+)', 'SCJ_SPRC=%s/\\1' % stu_code, record['sdd_scjv'])

        if record['sdd_scev']:
            record['sdd_scev'] = re.sub(r'SCE_SCJC=\d+/(\d+)', 'SCE_SCJC=%s/\\1' % stu_code, record['sdd_scev'])
            record['sdd_scev'] = re.sub(r'SCE_STUC=\d+', 'SCE_STUC=%s' % stu_code, record['sdd_scev'])
    
    for record in student['cam_sas']:
        record['spr_code'] = '%s/%s' % (stu_code, record['spr_code'].split('/')[1]) if record['spr_code'] else None
        record['sas_scnc'] = '%s/%s' % (stu_code, record['sas_scnc'].split('/')[1]) if record['sas_scnc'] else None
    
    for record in student['srs_snk']:
        record['snk_stuc'] = stu_code
    
    for record in student['srs_ssp']:
        record['ssp_stuc'] = stu_code
    
    for record in student['srs_tfn']:
        record['tfn_stuc'] = stu_code

    for record in student['srs_caf']:
        record['caf_stuc'] = stu_code
        record['caf_scjc'] = '%s/%s' % (stu_code, record['caf_scjc'].split('/')[1]) if record['caf_scjc'] else None
        record['caf_refn'] = unicode(uuid.uuid4())
    
    for record in student['srs_vis']:
        record['vis_stuc'] = stu_code
    
    for record in student['srs_rds']:
        record['rds_scjc'] = '%s/%s' % (stu_code, record['rds_scjc'].split('/')[1]) if record['rds_scjc'] else None
        record['rds_stuc'] = stu_code
    
    for record in student['srs_ssd']:
        record['ssd_scjc'] = '%s/%s' % (stu_code, record['ssd_scjc'].split('/')[1]) if record['ssd_scjc'] else None
    
    for record in student['srs_rdx']:
        record['rdx_scjc'] = '%s/%s' % (stu_code, record['rdx_scjc'].split('/')[1]) if record['rdx_scjc'] else None

def set_ipu_code(student, ipu_code):
    if student['srs_ipu']:
        student['srs_ipu']['ipu_code'] = ipu_code

    if student['men_mus']:
        student['men_mus']['mus_muac'] = ipu_code
        student['men_mus']['mus_valu'] = re.sub(r'MUA_CODE=IPU\d+', 'MUA_CODE=%s' % ipu_code, student['men_mus']['mus_valu'])
        student['men_mus']['mus_valu'] = re.sub(r'IPU_CODE=IPU\d+', 'IPU_CODE=%s' % ipu_code, student['men_mus']['mus_valu'])

    if student['srs_ipr']:
        student['srs_ipr']['ipr_ipuc'] = ipu_code

    for record in student['men_mre']:
        if record['mre_mrcc'] == 'IPU':
            record['mre_code'] = ipu_code

    for record in student['men_mua']:
        if record['mua_udf1'] == 'IPU':
            record['mua_code'] = ipu_code

    for record in student['srs_ipf']:
        record['ipf_ipuc'] = ipu_code
        if record['ipf_code'] == 'IPU_CODE':
            record['ipf_valu'] = ipu_code

    for record in student['srs_iprq']:
        record['iprq_ipuc'] = ipu_code

    for record in student['srs_ipa']:
        record['ipa_ipuc'] = ipu_code
    
    for record in student['srs_cap']:
        record['cap_ipuc'] = ipu_code

def import_student(student):
    for key, value in student.iteritems():
        
        if type(value) is dict:
            sits_utils.db_insert_multiple_records(key, [value])
        elif type(value) is list:
            sits_utils.db_insert_multiple_records(key, value)
    
class CustomEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return {
                '_type': 'datetime',
                'value': obj.isoformat()
            }
        return super(CustomEncoder, self).default(obj)


class CustomDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if '_type' not in obj:
            return obj
        if obj['_type'] == 'datetime':
            try:
                return datetime.datetime.strptime(obj['value'], '%Y-%m-%dT%H:%M:%S')
            except:
                return None
        return obj

def write_student_to_file(student, filename):
    if student['srs_ipf']:
        student['srs_ipf'] = sorted(student['srs_ipf'], key=lambda record: record['ipf_code']) 
    
    if student['srs_iprq']:
        student['srs_iprq'] = sorted(student['srs_iprq'], key=lambda record: record['iprq_ipqc']) 
    
    with open(filename, 'w') as outfile:
        json.dump(student, outfile, indent=4, sort_keys=True, cls=CustomEncoder)

def copy_student(stu_code, new_identifier):
    student = get_student(stu_code)
    set_student_identifier(student, new_identifier)
    import_student(student)

def read_student_from_file(filename):
    with open(filename, 'r') as infile:
        student = json.load(infile, cls=CustomDecoder)
        return student

def set_student_identifier(student, identifier):
    set_mst_code(student, '%012d' % (identifier))
    set_stu_code(student, '%09d' % (identifier))
    set_ipu_code(student, 'IPU%09d' % (identifier))

def set_student_forename1(student, name):
    name = unicode(name) or ''

    student['srs_mst']['mst_fnm1'] = name.upper()
    student['srs_mst']['mst_mfn1'] = name.capitalize()
    student['srs_mst']['mst_mfus'] = name.capitalize()
    student['srs_mst']['mst_init'] = name.upper()[:1]

    if student['srs_mst']['mst_surn']:
        student['srs_mst']['mst_name'] = '%s %s' % (name.capitalize(), student['srs_mst']['mst_surn'].capitalize())
    else:
        student['srs_mst']['mst_name'] = name.capitalize()
    
    student['ins_stu']['stu_fnm1'] = student['srs_mst']['mst_fnm1']
    student['ins_stu']['stu_name'] = student['srs_mst']['mst_name']
    student['ins_stu']['stu_init'] = student['srs_mst']['mst_init']

    if student['srs_ipr']:
        student['srs_ipr']['ipr_fnm1'] = student['srs_mst']['mst_fnm1']
        student['srs_ipr']['ipr_init'] = student['srs_mst']['mst_init']
    
    if student['srs_ipu']:
        student['srs_ipu']['ipu_fnm1'] = student['srs_mst']['mst_fnm1']
        student['srs_ipu']['ipu_init'] = student['srs_mst']['mst_init']

    if student['men_mus']:
        student['men_mus']['mus_valu'] = re.sub(r'USR_NAME=\w+', 'USR_NAME=%s' % student['srs_mst']['mst_name'], student['men_mus']['mus_valu'])

    for record in student['men_mua']:
        record['mua_name'] = student['srs_mst']['mst_name']
        
        if record['mua_udf1'] == 'IPU':
            record['mua_alis'] = student['srs_mst']['mst_fnm1']
    
    for record in student['srs_ipf']:
        if record['ipf_code'] == 'IPQ_APONOFFN':
            record['ipf_valu'] = student['srs_mst']['mst_fnm1']
        elif record['ipf_code'] == 'IPR_FNM1':
            record['ipf_valu'] = student['srs_mst']['mst_mfn1']
        elif record['ipf_code'] == 'APONOFFN':
            record['ipf_valu'] = student['srs_mst']['mst_mfn1']
    
    for record in student['srs_iprq']:
        if record['iprq_ipqc'] == 'APONOFFN':
            record['iprq_resp'] = student['srs_mst']['mst_fnm1']

    for record in student['ins_spr']:
        record['spr_fnm1'] = student['srs_mst']['mst_fnm1']

def set_student_forename2(student, name):
    name = unicode(name) or ''

def set_student_forename3(student, name):
    name = unicode(name) or ''

def set_student_forename_used(student, name):
    name = unicode(name) or ''
    
    student['srs_mst']['mst_fusd'] = name.upper()
    student['ins_stu']['stu_fusd'] = student['srs_mst']['mst_fusd']

    if student['srs_ipr']:
        student['srs_ipr']['ipr_fusd'] = student['srs_mst']['mst_fusd']

    if student['srs_ipu']:
        student['srs_ipu']['ipu_fusd'] = student['srs_mst']['mst_fusd']

    for record in student['srs_ipf']:
        if record['ipf_code'] == 'IPR_FUSD':
            record['ipf_valu'] = name.capitalize()

    for record in student['ins_spr']:
        record['spr_snam'] = student['srs_mst']['mst_fusd']

def set_student_surname(student, name):
    name = unicode(name)[:15] or ''
    
    student['srs_mst']['mst_surn'] = name.upper()
    student['srs_mst']['mst_msur'] = name.capitalize()
    student['srs_mst']['mst_name'] = '%s %s' % (student['srs_mst']['mst_fnm1'].capitalize(), name.capitalize())
    
    student['srs_mst']['mst_sndx'] = sits_utils.get_soundex(name)
    student['srs_mst']['mst_dmp1'], student['srs_mst']['mst_dmp2'] = sits_utils.get_double_metaphone(name)

    student['ins_stu']['stu_surn'] = student['srs_mst']['mst_surn']
    student['ins_stu']['stu_name'] = student['srs_mst']['mst_name']

    if student['srs_ipr']:
        student['srs_ipr']['ipr_surn'] = name.upper()
    
    if student['srs_ipu']:
        student['srs_ipu']['ipu_surn'] = student['srs_mst']['mst_surn']

    if student['men_mus']:
        student['men_mus']['mus_valu'] = re.sub(r'USR_NAME=[A-Za-z ]+', 'USR_NAME=%s' % student['srs_mst']['mst_name'], student['men_mus']['mus_valu'])

    for record in student['men_mua']:
        record['mua_name'] = student['ins_stu']['stu_name']

    for record in student['srs_ipf']:
        if record['ipf_code'] == 'IPR_SURN':
            record['ipf_valu'] = name.capitalize()

    for record in student['cam_sas']:
        record['sas_surn'] = student['srs_mst']['mst_surn']
    
    for record in student['cam_smo']:
        record['smo_surn'] = student['srs_mst']['mst_surn']

    for record in student['cam_sms']:
        record['sms_surn'] = student['srs_mst']['mst_surn']
    
    for record in student['ins_spr']:
        record['spr_name'] = student['srs_mst']['mst_surn']

    refresh_student_sortname(student)

def refresh_student_sortname(student):
    student['srs_mst']['mst_srtn'] = student['srs_mst']['mst_surn'][:14] + student['srs_mst']['mst_fnm1'][:1]
    student['ins_stu']['stu_srtn'] = student['srs_mst']['mst_srtn']

    if student['srs_ipu']:
        student['srs_ipu']['ipu_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_apf']:
        record['apf_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_cap']:
        record['cap_srtn'] = student['srs_mst']['mst_srtn']
    
    for record in student['srs_fcl']:
        record['fcl_srtn'] = student['srs_mst']['mst_srtn']
    
    for record in student['cam_srh']:
        record['srh_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_cdl']:
        record['cdl_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_fdu']:
        record['fdu_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_fdv']:
        record['fdv_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_sce']:
        record['sce_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['srs_scj']:
        record['scj_srtn'] = student['srs_mst']['mst_srtn']

    for record in student['ins_spr']:
        record['spr_surn'] = student['srs_mst']['mst_srtn']
    
    for record in student['cam_sdd']:
        if record['sdd_scjv']:
            record['sdd_scjv'] = re.sub(r'SCE_SRTN=[\w\s]+', 'SCE_SRTN=%s' % student['srs_mst']['mst_srtn'], record['sdd_scjv'])

        if record['sdd_scev']:
            record['sdd_scev'] = re.sub(r'SCJ_SRTN=[\w\s]+', 'SCJ_SRTN=%s' % student['srs_mst']['mst_srtn'], record['sdd_scev'])
        
        if record['sdd_sprv']:
            record['sdd_sprv'] = re.sub(r'SPR_SURN=[\w\s]+', 'SPR_SURN=%s' % student['srs_mst']['mst_srtn'], record['sdd_sprv'])

def set_student_email(student, email):
    email = unicode(email) or ''

    student['ins_stu']['stu_caem'] = email
    student['ins_stu']['stu_haem'] = email
    student['ins_stu']['stu_inem'] = email

    if student['srs_ipr']:
        student['srs_ipr']['ipr_caem'] = email
        student['srs_ipr']['ipr_haem'] = email

    if student['srs_ipu']:
        student['srs_ipu']['ipu_caem'] = email
        student['srs_ipu']['ipu_haem'] = email

    for record in student['men_add']:
        record['add_emad'] = email
    
    for record in student['men_mua']:
        if record['mua_udf1'] == 'STU':
            record['mua_emad'] = email
    
    for record in student['srs_ipf']:
        if record['ipf_code'] == 'IPR_HAEM':
            record['ipf_valu'] = email

def set_programme_occurrence(student, ipp_code, ipo_seqn):
    ipp = sits_utils.get_single_row('srs_ipp', None, {'ipp_code': ipp_code})
    ipo = sits_utils.get_single_row('srs_ipo', None, {'ipo_ippc': ipp['ipp_code'], 'ipo_seqn': ipo_seqn})
    mcr = sits_utils.get_single_row('srs_mcr', None, {'mcr_code': ipp_code})
    
    start_year = ipo['ipo_ayrc']
    start_month = '02' if (int(start_year) % 4 == 0 and int(start_year) % 100 != 0) else '03'
    department = ipp['ipp_dptc']
    faculty = ipp['ipp_facc']
    course = ipo['ipo_srce']
    programme = ipp['ipp_prgc']
    route = ipp['ipp_rouc']
    admissions_course = mcr['mcr_code']
    external_subject = mcr['mcr_udf4']

    if student['srs_ipr']:
        student['srs_ipr']['ipr_ippc'] = ipp_code
        student['srs_ipr']['ipr_ipos'] = ipo_seqn
        student['srs_ipr']['ipr_ayrc'] = start_year
        student['srs_ipr']['ipr_dptc'] = department

    for record in student['srs_ipa']:
        record['ipa_ippc'] = ipp_code
        record['ipa_ipos'] = ipo_seqn

    for record in student['srs_ipf']:
        record['ipf_ippc'] = ipp_code
        record['ipf_ipos'] = ipo_seqn
        
        if record['ipf_code'] == 'IPP_CODE':
            record['ipf_valu'] = ipp_code

        if record['ipf_code'] == 'IPO_SEQN':
            record['ipf_valu'] = ipo_seqn
        
        if record['ipf_code'] == 'IPQ_APONLMCR':
            record['ipf_valu'] = admissions_course

    for record in student['srs_iprq']:
        record['iprq_iprc'] = ipp_code
        record['iprq_iprs'] = ipo_seqn
        
        if record['iprq_ipqc'] == 'APONLMCR':
            record['iprq_resp'] = admissions_course
    
    for record in student['srs_cap']:
        record['cap_ippc'] = ipp_code
        record['cap_ipos'] = ipo_seqn
        record['cap_ayrc'] = start_year
        record['cap_mthc'] = start_month
        record['cap_crsc'] = course
        record['cap_dptc'] = department
        record['cap_facc'] = faculty
        record['cap_mcrc'] = admissions_course
        record['cap_prgc'] = programme
        record['cap_rouc'] = route
        record['cap_udf4'] = ipp_code
        record['cap_udfc'] = external_subject
    
    for record in student['srs_drl']:
        record['drl_ayrc'] = start_year
        record['drl_mthc'] = start_month
        record['drl_crsc'] = course
        record['drl_mcrc'] = admissions_course
        record['drl_prgc'] = programme
        record['drl_rouc'] = route

    for record in student['srs_cdl']:
        record['cdl_dptc'] = department
        record['cdl_mcrc'] = ipp_code
        record['cdl_udf3'] = faculty
    
    for record in student['srs_prf']:
        record['prf_crsc'] = course
        record['prf_mcrc'] = admissions_course
    
    for record in student['srs_scc']:
        record['scc_mcrc'] = admissions_course

def set_record_date(student, date_string):
    date = parser.parse(date_string)
    sits_date = {'_type': 'datetime', 'value': date.strftime('%Y-%m-%dT00:00:00')}
    
    if student['srs_mst']:
        student['srs_mst']['mst_updd'] = sits_date
    
    if student['ins_stu']:
        student['ins_stu']['stu_updd'] = sits_date
    
    if student['men_mus']:
        student['men_mus']['mus_updd'] = sits_date

    if student['srs_ipr']:
        student['srs_ipr']['ipr_updd'] = sits_date
        student['srs_ipr']['ipr_date'] = sits_date
        student['srs_ipr']['ipr_datt'] = sits_date

    for record in student['men_add']:
        record['add_begd'] = sits_date
        record['add_updd'] = sits_date
    
    for record in student['men_mre']:
        record['mre_cred'] = sits_date
        record['mre_updd'] = sits_date

    for record in student['men_mua']:
        record['mua_cred'] = sits_date
        record['mua_begd'] = sits_date
        record['mua_modd'] = sits_date
        
        if record['mua_lpad']:
            record['mua_lpad'] = sits_date

        if record['mua_used']:
            record['mua_used'] = sits_date
        
        if record['mua_expd']:
            record['mua_expd'] = {'_type': 'datetime', 'value': (date + relativedelta(years=3)).strftime('%Y-%m-%dT00:00:00')} 
    
    for record in student['srs_apf']:
        record['apf_recd'] = sits_date
        record['apf_reld'] = sits_date

    for record in student['srs_cap']:
        record['cap_crtd'] = sits_date
        record['cap_agoe'] = relativedelta(date, student['ins_stu']['stu_dob']).years

    for record in student['srs_cdl']:
        record['cdl_crdd'] = sits_date
        record['cdl_outd'] = sits_date

    for record in student['srs_drl']:
        record['drl_chgd'] = sits_date

    for record in student['srs_fcl']:
        record['fcl_chgd'] = sits_date
        record['fcl_clrd'] = sits_date

    for record in student['srs_ipa']:
        record['ipa_date'] = sits_date

    for record in student['srs_prf']:
        record['prf_cred'] = sits_date
        record['prf_orgd'] = sits_date
        record['prf_updd'] = sits_date

    for record in student['srs_scc']:
        record['scc_cred'] = sits_date
        record['scc_reqd'] = sits_date
        
        if record['scc_cclc'] == 'AFDA01':
            record['scc_reqd'] = {'_type': 'datetime', 'value': date.strftime('%Y-%m-%dT00:00:00')}
        else:
            record['scc_reqd'] = {'_type': 'datetime', 'value': (date + relativedelta(weeks=2)).strftime('%Y-%m-%dT00:00:00')}

def set_student_unikey(student, unikey):
    student['ins_stu']['stu_isar'] = unikey
    
    for record in student['men_mua']:
        record['mua_extu'] = unikey

def bulk_import(student_list):
    records = {}
    
    for student in student_list:
        for key, value in student.iteritems():
        
            if type(value) is dict:
                d = records.get(key, [])
                d.append(value)
                records[key] = d
                
            elif type(value) is list:
                for item in value:
                    d = records.get(key, [])
                    d.append(item)
                    records[key] = d
    
    for table_name, rows in records.iteritems():
        sits_utils.db_insert_multiple_records(table_name, rows)

def remove_student_file_uploads(student):
    ipf_list = [x for x in student.get('srs_ipf') if x['ipf_code'] != 'FILEUPLOAD.FILES']
    student['srs_ipf'] = ipf_list if ipf_list else None

def randomize_student_addresses(student):
    address_suffixes = [ 'cad1', 'cad2', 'cad3', 'cad4', 'cad5', 'had1', 'had2', 'had3', 'had4', 'had5' ]
    phone_suffixes = [ 'cafn', 'cat2', 'cat3', 'ctel', 'hafn', 'hat2', 'hat3', 'htel' ]
    postcode_suffixes = [ 'capc', 'hapc' ]
    
    for key in student['ins_stu']:
        if key[-4:] in address_suffixes:
            student['ins_stu'][key] = generic_utils.random_string(10)
        elif key[-4:] in address_suffixes:
            student['ins_stu'][key] = generic_utils.random_number(8)
        elif key[-4:] in postcode_suffixes:
            student['ins_stu'][key] = generic_utils.random_number(5)
            
    for record in student['srs_ipf']:
        if record['ipf_code'][-4:] in address_suffixes:
            record['ipf_valu'] = generic_utils.random_string(10)
        elif record['ipf_code'][-4:] in phone_suffixes:
            record['ipf_valu'] = generic_utils.random_number(8)
        elif record['ipf_code'][-4:] in postcode_suffixes:
            record['ipf_valu'] = generic_utils.random_number(5)
    
    for record in student['men_add']:
        record['add_add1'] = generic_utils.random_string(10)
        record['add_add2'] = generic_utils.random_string(10)
        record['add_add3'] = generic_utils.random_string(10)
        record['add_add4'] = generic_utils.random_string(10)
        record['add_add5'] = generic_utils.random_string(10)
        record['add_pcod'] = generic_utils.random_number(5)
        record['add_teln'] = generic_utils.random_number(8)
        record['add_tel2'] = generic_utils.random_number(8)
        record['add_tel3'] = generic_utils.random_number(8)
    
    for record in student['srs_snk']:
        for key in record:
            if key[-4:] in address_suffixes:
                record[key] = generic_utils.random_string(10)
            elif key[-4:] in address_suffixes:
                record[key] = generic_utils.random_number(8)
            elif key[-4:] in postcode_suffixes:
                record[key] = generic_utils.random_number(5)

def set_student_dob(student, date):
    format_date = date.strftime('%d/%m/%Y')
    
    student['ins_stu']['stu_dob'] = date
    student['srs_mst']['mst_dob'] = date

    if student['srs_ipr']:
        student['srs_ipr']['ipr_dob'] = date
    
    if student['srs_ipu']:
        student['srs_ipu']['ipu_dob'] = date
    
    for record in student['ins_spr']:
        record['spr_dob'] = date
    
    for record in student['srs_ipf']:
        if record['ipf_code'] == 'IPR_DOB':
            record['ipf_valu'] = format_date

def scramble_student_snk(student):
    global sample_data
    
    for record in student['srs_snk']:
        record['snk_dob'] = None
        record['snk_fnm1'] = random.choice(sample_data['fnm1'])
        record['snk_surn'] = random.choice(sample_data['surn'])
        record['snk_udfj'] = record['snk_surn'].capitalize()

def scramble_student(student, identifier):
    global sample_data

    set_student_identifier(student, identifier)

    set_student_forename1(student, random.choice(sample_data['fnm1']))
    set_student_forename2(student, random.choice(sample_data['fnm2']))
    set_student_forename3(student, random.choice(sample_data['fnm3']))
    set_student_forename_used(student, random.choice(sample_data['fusd']))
    set_student_surname(student, random.choice(sample_data['surn']))
    set_student_email(student, 'student-email@example.com')
    set_student_unikey(student, student['ins_stu']['stu_code'])
    set_student_dob(student, generic_utils.random_date(1920, 2000))
    
    randomize_student_addresses(student)
    
    scramble_student_snk(student)
    
    remove_student_file_uploads(student)

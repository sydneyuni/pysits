#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import copy
import re

import cx_Oracle
import fuzzy

from utils import db_utils


SITS_DB_REGEX = re.compile(r'^[a-zA-Z0-9]{3,4}_[a-zA-Z0-9_]{3,7}$')
LOB_TYPES = ['BLOB', 'CLOB', 'NCLOB', 'RAW']

def _clean_name(field_name):
    '''Attempts to return a strictly SITS-friendly database name, or None if impossible.
    '''
    return field_name.lower() if SITS_DB_REGEX.match(field_name) else None

def _clean_fields(fields):
    '''Attempts to return an array of strictly SITS-friendly fields.
    '''
    sits_fields = []
    for f in fields or []:
        if SITS_DB_REGEX.match(f):
            sits_fields.append(f.lower())
    return sits_fields

def _clean_dictionary(dictionary):
    '''Attempts to return a dictionary with strictly SITS-friendly keys.
    '''
    sits_dictionary = {}
    for key, value in dictionary.iteritems():
        if SITS_DB_REGEX.match(key):
            sits_dictionary.update({key.lower():value})
    return sits_dictionary

def _create_where_sql(where_dict, table_name):
    '''Creates an SQL 'WHERE' clause for the given dictionary.
    Also wraps the column definition in 'to_char' if the parameter
    is a *LOB, to accommodate *LOB comparison.
    '''
    data_types = get_data_types(table_name)
    if where_dict:
        def f(x, y): return 'to_char(%s)' % x if data_types[x] in LOB_TYPES else x
        where_sql = ' AND '.join('%s=:%s' % (f(key, value), key) for key, value in where_dict.items())
        return ' WHERE ' + where_sql
    else:
        return ''

def get_single_field(table, field, retrieval_profile):
    '''Retrieves a single field from the database.
    '''
    field = _clean_name(field)
    result = get_rows(table, [field], retrieval_profile)
    if result and field in result[0]:
        return result[0].get(field)
    else:
        return None

def get_single_column(table, field, retrieval_profile):
    '''Retrieves an array of a single field from the database.
    '''
    field = _clean_name(field)
    result = get_rows(table, [field], retrieval_profile)
    if result:
        return [row.get(field) for row in result]
    else:
        return None

def get_rows(table, fields=None, retrieval_profile={}, preprocess_rows=True):
    '''Retrieves numerous rows from the database.
    '''
    sits_table = _clean_name(table)
    sits_fields = _clean_fields(fields)
    
    if not sits_table:
        return None
    
    fields_string = ','.join(sits_fields) if sits_fields else '*'
    sql = 'SELECT %s FROM %s' % (fields_string, sits_table)
    
    where_dict = _clean_dictionary(retrieval_profile)
    sql += _create_where_sql(where_dict, sits_table)
    
    return db_utils.fetch(sql, where_dict, preprocess_rows)

def get_single_row(table, fields=None, retrieval_profile=None):
    '''Retrieves a single row from the database.
    '''
    result = get_rows(table, fields, retrieval_profile)
    return result[0] if len(result) > 0 else None

def get_row_count(table, retrieval_profile):
    '''Gets the row count from the database.
    '''
    sits_table = _clean_name(table)
    if not sits_table:
        return None
    
    sql = 'SELECT COUNT(*) c FROM %s' % (sits_table)
    
    where_dict = _clean_dictionary(retrieval_profile)
    sql += _create_where_sql(where_dict, sits_table)
    
    results = db_utils.fetch(sql, where_dict)
    if results:
        return results[0].get('c')
    else:
        return None

def update_single_row(table, retrieval_profile, update_profile):
    '''Performs an update to a single row in the database.
    '''
    sits_table = _clean_name(table)
    where_dict = _clean_dictionary(retrieval_profile)
    set_dict = _clean_dictionary(update_profile)
    
    if not sits_table or not where_dict or not set_dict:
        return None
    
    row_count = get_row_count(sits_table, where_dict)
    if row_count != 1:
        return None
    
    update_sql = ', '.join('%s=:u_%s' % (key, key) for key in set_dict)
    sql = 'UPDATE %s SET %s' % (sits_table, update_sql)
    
    where_dict = _clean_dictionary(retrieval_profile)
    sql += _create_where_sql(where_dict, sits_table)

    update_dict = {}
    update_dict.update(where_dict)
    
    for key, value in set_dict.iteritems():
        update_dict.update({'u_' + key:value})
    
    db_utils.update(sql, update_dict)
    
def db_create_record(table, create_profile, verification_profile=None):
    '''Creates a single row in the database.
    '''
    sits_table = _clean_name(table)
    sits_create_profile = _clean_dictionary(create_profile)
    input_sizes = get_input_sizes(table)
    
    if not sits_table or not sits_create_profile:
        return None
    
    column_sql = ','.join(key for key in sits_create_profile)
    values_sql = ','.join(':%s' % (key) for key in sits_create_profile)
    
    sql = 'INSERT INTO %s (%s) VALUES (%s)' % (sits_table, column_sql, values_sql)
    
    db_utils.update(sql, sits_create_profile, input_sizes)
    row_count = get_row_count(table, verification_profile or create_profile)
    if row_count < 1:
        raise Exception('Failed to create %s record' % (table.upper()))

def db_insert_multiple_records(table, rows):
    sits_table = _clean_name(table)
    input_sizes = get_input_sizes(table)
    
    if not sits_table or not rows:
        return None

    for i in range(0, len(rows)):
        rows[i] = _clean_dictionary(rows[i])
    
    column_sql = ','.join(key for key in rows[0])
    values_sql = ','.join(':%s' % (key) for key in rows[0])
    
    sql = 'INSERT INTO %s (%s) VALUES (%s)' % (sits_table, column_sql, values_sql)
    
    db_utils.update_multiple(sql, rows, input_sizes)

def db_merge_multiple_records(table, rows):
    sits_table = _clean_name(table)
    input_sizes = get_input_sizes(table)
    
    if not sits_table or not rows:
        return None
    
    for i in range(0, len(rows)):
        rows[i] = _clean_dictionary(rows[i])
    
    # get the primary keys (for 'on' clause)
    (dct_code, ent_code) = get_dictionary_and_entity(table)
    pkeys = get_entity_field_list(dct_code, ent_code, True)

    # filter out primary keys (for 'update' clause) 
    column_list_complete = rows[0]
    column_list_updatable = [col for col in column_list_complete if col not in pkeys]
    
    select_values = ', '.join(':%s %s' % (column, column) for column in column_list_complete)
    on_keys = ' AND '.join('t.%s = v.%s' % (pkey, pkey) for pkey in pkeys)
    update_values = ', '.join('t.%s = v.%s' % (column, column) for column in column_list_updatable)
    insert_columns = ', '.join('%s' % column for column in column_list_complete)
    insert_values = ', '.join('v.%s' % column for column in column_list_complete)

    sql = 'MERGE INTO %s t ' % sits_table
    sql += 'USING (SELECT %s FROM dual) v ' % select_values
    sql += 'ON (%s) ' % on_keys
    sql += 'WHEN MATCHED THEN UPDATE SET %s ' % update_values
    sql += 'WHEN NOT MATCHED THEN INSERT (%s) VALUES (%s)' % (insert_columns, insert_values)
    
    db_utils.update_multiple(sql, rows, input_sizes)

def get_maximum_of_field(table, field, retrieval_profile):
    sits_table = _clean_name(table)
    sits_field = _clean_name(field)
    if not sits_table or not sits_field:
        return None
    
    sql = 'SELECT MAX(%s) m FROM %s' % (sits_field, sits_table)
    
    where_dict = _clean_dictionary(retrieval_profile)
    sql += _create_where_sql(where_dict, sits_table)
    
    results = db_utils.fetch(sql, where_dict)
    if results:
        return results[0].get('m')
    else:
        return None

def get_entity_field_list(dct_code, ent_code, pkeys_only=False):
    retrieval_profile = {'fld_dctc': dct_code.upper(), 'fld_entc': ent_code.upper()}
    where_filter = 'fld_dctc=:fld_dctc AND fld_entc=:fld_entc'
    
    if pkeys_only:
        where_filter += ' AND fld_indx=:fld_indx'
        retrieval_profile['fld_indx'] = 'P'
    
    sql = 'SELECT fld_code FROM men_fld WHERE %s ORDER BY fld_seqn' % where_filter
    rows = db_utils.fetch(sql, retrieval_profile)
    
    return [row['fld_code'].lower() for row in rows]

def get_entity_field_types(dct_code, ent_code):
    retrieval_profile = {'fld_indb': 'Y', 'fld_dctc': dct_code.upper(), 'fld_entc': ent_code.upper()}
    where_filter = 'fld_indb=:fld_indb AND fld_dctc=:fld_dctc AND fld_entc=:fld_entc'
    
    sql = 'SELECT fld_code,fld_indx,fld_type FROM men_fld WHERE %s ORDER BY fld_seqn' % where_filter
    rows = db_utils.fetch(sql, retrieval_profile)
    
    return dict((row['fld_code'].lower(), row) for row in rows)

def get_dictionary_and_entity(table_name):
    (ent_prfx, ent_ent1) = table_name.upper().split('_', 1)
    retrieval_profile = {'ent_prfx': ent_prfx + '_', 'ent_ent1': ent_ent1, 'ent_styp': 'Y'}
    row = get_single_row('men_ent', ['ent_dctc', 'ent_code'], retrieval_profile)
    if row is None:
        raise ValueError('Could not find %s_%s in MEN_ENT' % (table_name))
    return (row['ent_dctc'], row['ent_code'])

def get_super_dictionary_and_entity(dct_code, ent_code):
    retrieval_profile = {'ent_dctc': dct_code, 'ent_code': ent_code}
    row = get_single_row('men_ent', ['ent_dctc', 'ent_ent1'], retrieval_profile)
    if row is None:
        raise ValueError('Could not find %s.%s in MEN_ENT' % (dct_code, ent_code))
    return (row['ent_dctc'], row['ent_ent1'])

def get_table_name(dct_code, ent_code):
    retrieval_profile = {'ent_dctc': dct_code, 'ent_code': ent_code}
    row = get_single_row('men_ent', ['ent_prfx', 'ent_ent1'], retrieval_profile)
    if row is None:
        raise ValueError('Could not find %s.%s in MEN_ENT' % (dct_code, ent_code))
    return row.get('ent_prfx') + row.get('ent_ent1')

def get_soundex(string):
    sql = "SELECT SOUNDEX(:s) s FROM DUAL"
    rows = db_utils.fetch(sql, {'s': string})
    if rows:
        return rows[0].get('s')
    else:
        return None

def get_double_metaphone(string):
    dmeta = fuzzy.DMetaphone()
    string = string.encode('ascii','ignore') if string else string
    return tuple(dmeta(string))

def get_project_templates():
    prt_list = get_rows('men_prt')
    return dict((prt['prt_code'], prt) for prt in prt_list)

def _get_project_headers(prj_code):
    prj = get_rows('men_prj', [], {'prj_code': prj_code})
    pri_list = get_rows('men_pri', [], {'pri_prjc': prj_code})
    return {'men_prj': prj, 'men_pri': pri_list}

def get_project_records(prj_code):
    headers = _get_project_headers(prj_code)
    templates = get_project_templates()
    
    for pri in headers['men_pri']:
        if pri['pri_prtc']:
            t = templates[pri['pri_prtc']]
            erll = t['prt_erll'].split('=', 1)[1]
            print erll
            print
            
            print re.findall('([^\x15]+)(?:\x15\x1b)?', erll)
    
    return None

def get_data_types(table):
    if not get_data_types.CACHE:
        sql = "SELECT table_name, column_name, data_type FROM all_tab_columns"
        result = db_utils.fetch(sql)
        
        get_data_types.CACHE = {}
        for row in result:
            (table_name, column_name, data_type) = (row['table_name'].lower(), row['column_name'].lower(), row['data_type'])
            if table_name not in get_data_types.CACHE:
                get_data_types.CACHE[table_name] = {}
            get_data_types.CACHE[table_name][column_name] = data_type
    
    return get_data_types.CACHE.get(table.lower())
get_data_types.CACHE = None

def get_input_sizes(table):
    if not get_input_sizes.CACHE:
        
        data_type_sizes = {
            'BLOB': cx_Oracle.BLOB,
            'CHAR': cx_Oracle.FIXED_CHAR,
            'CLOB': cx_Oracle.CLOB,
            'DATE': cx_Oracle.DATETIME,
            'NCHAR': cx_Oracle.NCHAR,
            'NCLOB': cx_Oracle.NCLOB,
            'NUMBER': cx_Oracle.FIXED_CHAR,
            'NVARCHAR2': cx_Oracle.NCHAR,
            'RAW': cx_Oracle.BINARY,
            'VARCHAR2': cx_Oracle.NCHAR
        }
        
        # duplicate "data types" and swap out the data type for the Oracle size
        _ = get_data_types('') # ensure 'data types' cache is initialised
        get_input_sizes.CACHE = copy.deepcopy(get_data_types.CACHE)
        for table_name, columns in get_input_sizes.CACHE.iteritems():
            for column_name, data_type in columns.iteritems():
                get_input_sizes.CACHE[table_name][column_name] = data_type_sizes.get(data_type)
    
    return get_input_sizes.CACHE.get(table.lower())
get_input_sizes.CACHE = None

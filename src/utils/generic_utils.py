#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import random
import string
import time
import datetime


def random_string(length):
    return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in xrange(length))

def random_number(length):
    return ''.join(str(random.SystemRandom().randint(0, 9)) for _ in xrange(length))

def random_date(start_year, end_year):
    start_date = datetime.date(start_year, 1, 1).toordinal()
    end_date = datetime.date(end_year, 12, 31).toordinal()
    return datetime.date.fromordinal(random.randint(start_date, end_date))

def timing(f):
    def wrap(*args):
        time1 = time.time()
        ret = f(*args)
        time2 = time.time()
        print '%s function took %0.3f ms' % (f.func_name, (time2-time1)*1000.0)
        return ret
    return wrap

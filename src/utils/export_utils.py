#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import hashlib
import os
import traceback

from lxml import etree

from utils import sits_utils, project_utils, db_utils
from utils.generic_utils import timing


MAX_WAIT_TIME = 60
FIELD_EXPORT_BLACKLIST = ['MENSYS.SLP.slp_used', 'MENSYS.SRL.srl_batd']

def export_table_to_single_xml(table_name, file_path):
    records = sits_utils.get_rows(table_name)
    
    xml_root = etree.Element('project')
    
    xml_table = etree.SubElement(xml_root, 'table', name=table_name) 
    
    (dct_code, ent_code) = sits_utils.get_dictionary_and_entity(table_name)
    fld_list = sits_utils.get_entity_field_list(dct_code, ent_code)
    
    for row in records:
        xml_record = etree.SubElement(xml_table, 'record')
        for fld in fld_list:
            value = row.get(fld, '')
            field_string = '%s.%s.%s' % (dct_code, ent_code, fld)
            if field_string in FIELD_EXPORT_BLACKLIST:
                etree.SubElement(xml_record, fld).text = None
            elif value is None:
                etree.SubElement(xml_record, fld).text = None
            elif isinstance(value, basestring):
                etree.SubElement(xml_record, fld).text = value.encode('unicode_escape')
            else:
                etree.SubElement(xml_record, fld).text = unicode(value)
                    
    tree = etree.ElementTree(xml_root)
    tree.write(file_path, pretty_print=True)

def export_table_to_single_xml_stream(table_name, file_path):
    with etree.xmlfile(file_path, encoding='utf-8') as xf:
        with xf.element('project'):
            with xf.element('table', name=table_name):
            
                (dct_code, ent_code) = sits_utils.get_dictionary_and_entity(table_name)
                fld_list = sits_utils.get_entity_field_list(dct_code, ent_code)
                
                cursor = sits_utils.get_rows(table_name, preprocess_rows=False)
                
                for row in cursor:
                    record = db_utils.process_row(row, cursor)
                    xml_record = etree.Element('record')
                    
                    for fld in fld_list:
                        value = record.get(fld, '')
                        field_string = '%s.%s.%s' % (dct_code, ent_code, fld)
                        if field_string in FIELD_EXPORT_BLACKLIST:
                            etree.SubElement(xml_record, fld).text = None
                        elif value is None:
                            etree.SubElement(xml_record, fld).text = None
                        elif isinstance(value, basestring):
                            etree.SubElement(xml_record, fld).text = value.encode('unicode_escape')
                        else:
                            etree.SubElement(xml_record, fld).text = unicode(value)
                
                    xf.write(xml_record)
                
                cursor.close()

def export_project_to_xml_files(project, directory_path):
    records = project_utils.get_project_records(project)
    
    for table_name, rows in records.iteritems():
        
        directory = os.path.join(directory_path, project, table_name)
        if not os.path.exists(directory):
            os.makedirs(directory)
            
        (dct_code, ent_code) = sits_utils.get_dictionary_and_entity(table_name)
        pkeys = sits_utils.get_entity_field_list(dct_code, ent_code, True)
        
        for row in rows:
            pkey_string = '_'.join([str(row[key]) for key in pkeys])
            filename = "%s.xml" % hashlib.md5(pkey_string).hexdigest()
            
            root = root = etree.Element(table_name)
            for key, value in row.iteritems():
                try:
                    etree.SubElement(root, key).text = unicode(value) if value else None
                except ValueError:
                    etree.SubElement(root, key).text = value.encode('unicode_escape')
                    
            tree = etree.ElementTree(root)
            
            file_path = os.path.join(directory, filename)
            tree.write(file_path, pretty_print=True)

@timing
def export_all_projects_to_xml(directory_path):
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)
    
    prj_records = sits_utils.get_rows('men_prj')

    for prj in prj_records:
        file_path = os.path.join(directory_path, "%s.xml" % prj['prj_code'])
        try:
            export_project_to_single_xml(prj['prj_code'], file_path)
        except Exception:
            print 'Error while exporting project: %s' % prj['prj_code']
            traceback.print_exc()

def export_project_to_single_xml(project, file_path):
    records = project_utils.get_project_records(project)
    
    xml_root = etree.Element('project', prj_code=project)
    
    for table_name in sorted(records.keys()):
        
        xml_table = etree.SubElement(xml_root, 'table', name=table_name) 
        
        (dct_code, ent_code) = sits_utils.get_dictionary_and_entity(table_name)
        fld_list = sits_utils.get_entity_field_list(dct_code, ent_code)
        
        for row in records.get(table_name):
            xml_record = etree.SubElement(xml_table, 'record')
            for fld in fld_list:
                value = row.get(fld, '')
                field_string = '%s.%s.%s' % (dct_code, ent_code, fld)
                if field_string in FIELD_EXPORT_BLACKLIST:
                    etree.SubElement(xml_record, fld).text = None
                elif value is None:
                    etree.SubElement(xml_record, fld).text = None
                elif isinstance(value, basestring):
                    etree.SubElement(xml_record, fld).text = value.encode('unicode_escape')
                else:
                    etree.SubElement(xml_record, fld).text = unicode(value)
                    
    tree = etree.ElementTree(xml_root)
    tree.write(file_path, pretty_print=True)

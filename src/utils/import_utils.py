#
# Copyright 2016 University of Sydney
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import datetime
import glob
import os
import sys
import traceback

from lxml import etree

from utils import sits_utils


MAX_WAIT_TIME = 60

def _load_records_from_xml(file_path, records={}):
    with open(file_path, 'r') as f:
        xml_tree = etree.parse(f)
    
    xml_root = xml_tree.getroot()
    
    # read from XML file
    for xml_table in xml_root.iter("table"):
        table_name = xml_table.attrib['name']
        if table_name not in records:
            records[table_name] = []
        
        (dct_code, ent_code) = sits_utils.get_dictionary_and_entity(table_name)
        fld_types = sits_utils.get_entity_field_types(dct_code, ent_code)
        
        for xml_record in xml_table.iter("record"):
            row = {}
            for xml_field in xml_record.iter(tag=etree.Element):
                if xml_field == xml_record or xml_field.tag not in fld_types:
                    continue
                field_type = fld_types[xml_field.tag]['fld_type']

                if xml_field.text is None:
                    row[xml_field.tag] = None
                elif field_type in ('D', 'E', 'L', 'T'):
                    row[xml_field.tag] = datetime.datetime.strptime(xml_field.text, '%Y-%m-%d %H:%M:%S')
                else:
                    row[xml_field.tag] = xml_field.text.decode('unicode_escape')
                
            # Don't add empty rows
            if row:
                records[table_name].append(row)
    
    return records

def import_project_xml_file(file_path):
    records = _load_records_from_xml(file_path)

    # import into database
    for table_name in sorted(records.keys()):
        rows = records.get(table_name)
        sits_utils.db_merge_multiple_records(table_name, rows)

def import_project_xml_directory(dir_path):
    xml_file_list = glob.glob(os.path.join(dir_path, '*.xml'))
    
    max_size = 20
    file_list_chunked = [xml_file_list[i:i + max_size] for i in xrange(0, len(xml_file_list), max_size)]
    
    i = 1
    for chunk in file_list_chunked:
        _import_project_xml_chunk(chunk, i)
        i += 1

def _import_project_xml_chunk(xml_file_list, i):
    try:
        records = {}
            
        for xml_file in xml_file_list:
            if os.path.isfile(xml_file):
                print 'Loading (chunk %d): %s' % (i, xml_file)
                sys.stdout.flush()
                _load_records_from_xml(xml_file, records)
        
        print 'Importing records (chunk %d)' % i
        sys.stdout.flush()
        for table_name in sorted(records.keys()):
            rows = records.get(table_name)
            sits_utils.db_merge_multiple_records(table_name, rows)
    except Exception:
        traceback.print_exc()
        raise

def import_table_xml_file(xml_file):
    records = _load_records_from_xml(xml_file)
    
    # import into database
    for table_name in sorted(records.keys()):
        rows = records.get(table_name)
        
        max_size = 20
        rows_chunked = [rows[i:i + max_size] for i in xrange(0, len(rows), max_size)]
        
        for chunk in rows_chunked:
            sits_utils.db_merge_multiple_records(table_name, chunk)
